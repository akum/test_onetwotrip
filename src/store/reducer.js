import {combineReducers} from 'redux';

import flightsReducer, {moduleName as flights} from 'dugs/flights';

export default combineReducers({
	[flights]:flightsReducer,
})