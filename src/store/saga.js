import {all} from 'redux-saga/effects';

import {saga as flightsSaga} from 'dugs/flights';

export default function * rootSaga () {
	yield all([
		flightsSaga()
	])
}