import {createStore, applyMiddleware, compose} from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import reducer from './reducer';
import rootSaga from './saga';

let extansion = compose;

//Для расширени
if(typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
	extansion = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({});
}

const sagaMiddleware = createSagaMiddleware();

const enchancer =  extansion(applyMiddleware(sagaMiddleware, logger));

const store = createStore(
	reducer,
	enchancer
);

sagaMiddleware.run(rootSaga);

export default store;