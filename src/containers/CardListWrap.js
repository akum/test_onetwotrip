import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import {filteredList} from 'dugs/flights';
import CardList from 'components/elements/CardList';

class CardListWrap extends Component {
	render() {
		return (
			<Fragment>
				<CardList list={this.props.listFlight}/>
			</Fragment>
		)
	};
}

CardListWrap.defaultProps = {
	listFlight: []
};

export default connect(state => ({
	listFlight : filteredList(state).valueSeq().toArray()
}), {})(CardListWrap);

