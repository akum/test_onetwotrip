import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";

import {getCarierUniqSelector, activeSelect} from 'dugs/flights';
import SelectEl from 'components/elements/Select';

class Select extends Component {
	render () {
		return (
			<Fragment>
				<SelectEl defaultValue={0} list={this.props.carierListSelect} selectedEv={this.props.activeSelect}/>
			</Fragment>
		);
	}
}

Select.propTypes = {
	carierListSelect : PropTypes.array
};


Select.defaultProps= {
	carierListSelect : []
};


export default connect(state => ({
	carierListSelect : [{value:'all', label:'all'} , ...getCarierUniqSelector(state).map((carrier) => ({value:carrier, label: carrier}))],
}), {activeSelect})(Select);

