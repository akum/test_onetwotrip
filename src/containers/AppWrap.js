import React, { Component, Fragment } from 'react';
import {connect} from 'react-redux';
import {getList} from 'dugs/flights';

class AppWrap extends Component {
	componentDidMount () {
		this.props.getList();
	}
	render () {
		return (
			<Fragment>
				{this.props.children}
			</Fragment>
		);
	}
}

AppWrap.propTypes = {};

export default connect(null, {getList})(AppWrap);
