import React, { Component } from 'react';
import styled from 'styled-components';
// import PropTypes from 'prop-types';

import style from 'components/utils';

class Title extends Component {
	render () {
		return (
			<TitleStyle styleCustom={this.props.styleCustom}>
				<span>{this.props.children}</span>
			</TitleStyle>
		);
	}
}

// Title.propTypes = {};

export default Title;

const TitleStyle = styled.p`
	color: ${style.color.bg};
	margin-right: auto;
    ${(props) => props.styleCustom}
`;