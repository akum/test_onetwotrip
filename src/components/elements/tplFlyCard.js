import React, { Fragment } from 'react';
import Title from 'components/elements/Title';
import TextContent from 'components/elements/TextContent';
export const Header = (props) => (
	<Fragment>
		<Title styleCustom={'margin-left:16px'}>{props.data.carrier}</Title>
		<Title styleCustom={'margin-right:16px'}>{`в пути  ${props.data.diffTime}`}</Title>
	</Fragment>
);

export const TopContent = (props) => (
	<Fragment>
		<TextContent>{`Вылет из ${props.data.direction.from}`}</TextContent>
		<TextContent>{props.data.departure}</TextContent>
	</Fragment>
);

export const BottomContent = (props) => (
	<Fragment>
		<TextContent>{`прилет в ${props.data.direction.to}`}</TextContent>
		<TextContent>{props.data.arrival}</TextContent>
	</Fragment>
);