import React, { Component } from 'react';
import styled from 'styled-components';
import style from 'components/utils';
import PropTypes from 'prop-types';

class Panel extends Component {
	render () {
		return (
			<PanelStyle customStyle={this.props.customStyle}>
				{this.props.children}
			</PanelStyle>
		);
	}
}

Panel.propTypes = {
	customStyle: PropTypes.string
};
Panel.defaultProps = {
	customStyle: ''
};
export default Panel;

const PanelStyle = styled.div`
	display: flex;
    justify-content: flex-start;
    align-items: stretch;
    flex-direction: column;
    
    padding: 15px 15px;
    background: ${style.color.bg};
    box-shadow: 0 0px 11px 0px ${style.color.shadow};
  	
    ${props => props.customStyle}
	
`;