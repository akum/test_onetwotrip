import React, { Component } from 'react';
import Select from 'react-select'
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Panel from './Panel';
import style from 'components/utils';


const customStylesSelect = {
	menu: (base) => ({
		...base,
		marginTop: 5,
		borderRadius: 0
	}),
	
	container : (base) => ({
		...base,
		minHeight: 30,
		height: 30
	}),
	control : (base) => ({
		...base,
		minHeight:30,
	}),
	indicatorsContainer: (base) => ({
		...base,
		height: 30
	}),
	valueContainer: (base) => ({
		...base,
		height: 30
	})
};

class SelectEl extends Component {
	static propTypes = {
		isLabel: PropTypes.bool,
		label: PropTypes.string,
		list: PropTypes.array,
		defaultSelected: PropTypes.number,
		activeSelect: PropTypes.func
	};
	
	
	static defaultProps = {
		isLabel: true,
		label: 'Выберите carrier',
		defaultSelected: null,
		list: [{value: 'default', label:'default'}],
		selectedEv: () => {}
	};
	
	
	state = {
		selected: this.props.valueDefault || '',
	};
	
	
	eventSelect = (selected) => {
		
		this.setState({selected});
		this.props.selectedEv(selected.value);
	};
	
	defaultOptions = () =>{
		this.setState(this.props.list[1]);
	};
	
	render () {
		return (
			<Panel customStyle='flex: 1 0;min-width: 220px;height: auto;position: fixed;'>
				{this.props.isLabel && <LabelStyle htmlFor="">{this.props.label}</LabelStyle>}
				<Select
					defaultValue={this.props.list[0]}
					loadOptions={this.defaultOptions}
					onChange={this.eventSelect}
					clearable={false}
					options={this.props.list}
					styles={customStylesSelect}
				/>
			</Panel>
		);
	}
}

export default SelectEl;

const LabelStyle = styled.label`
	${style.indents.margin.bottom};
`;
