import React, { Component, Fragment } from 'react';
import styled from 'styled-components';

class Devider extends Component {
	render () {
		return (
			<Fragment>
				<DeviderStyle />
			</Fragment>
		);
	}
}

Devider.propTypes = {};

export default Devider;

const DeviderStyle = styled.span`
	width: 60%;
    align-self: center;
	display: block;
    border-bottom: 2px solid #eee;
    box-sizing: content-box;
    padding-top: 2px;
`;