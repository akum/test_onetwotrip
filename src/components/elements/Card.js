import React, { Component } from 'react';
import styled from 'styled-components';
// import PropTypes from 'prop-types';
import style from 'components/utils';

import Devider from "./Devider";

class Card extends Component {
	render () {
		const {Header, TopContent, BottomContent } = this.props;
		return (
			<ContainerCardStyle>
				<HeaderStyle>
					<Header />
				</HeaderStyle>
				
				<TopContentStyle>
					<TopContent />
				</TopContentStyle>
				<Devider />
				<BottomContentStyle>
					<BottomContent />
				</BottomContentStyle>
			</ContainerCardStyle>
		);
	}
}

// Card.propTypes = {
//
// };
// Card.defaultProps = {
//
// };

export default Card;

const ContainerCardStyle = styled.div`
    display: flex;
    flex-direction: column;
    
    flex: 1 1 0;
	min-width: 250px;
	margin: 10px;
	border-radius: 4px;
	box-shadow: 0 1px 3px 0 ${style.color.shadow}, 0 1px 2px 0 ${style.color.shadow};
`;

const HeaderStyle = styled.div`
	width:100%;
	background: ${style.color.bg2};
	border-top-left-radius: 4px;
	border-top-right-radius: 4px;
	display: flex;
	height: 30px;
	font-size: 14px;
	justify-content: center;
	align-items: center;
`;

const CommonСard = styled.div`
	margin: 15px
`;

const BottomContentStyle = CommonСard.withComponent(styled.div`
	background-color:${style.color.bg};
`);

const TopContentStyle = CommonСard.withComponent(styled.div``);