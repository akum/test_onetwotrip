import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import {Header, TopContent, BottomContent} from 'components/elements/tplFlyCard'
import Card from 'components/elements/Card';
import Panel from 'components/elements/Panel';

class CardList extends Component {
	
	CardEl = () => this.props.list.map((val)=> {
		return <Card key={val.id}
			 Header={() => <Header data={val}/> }
			 TopContent={ ()=> <TopContent data={val} /> }
			 BottomContent={ ()=> <BottomContent data={val} />  }
			>
		</Card>
	});
	
	render () {
		return (
			<Panel customStyle='width: 100%;flex: 0 1 100%; margin-left: 255px;'>
				<ListWrapStyle>
					{this.CardEl()}
				</ListWrapStyle>
			</Panel>
		);
	}
}

CardList.propTypes = {
	list: PropTypes.array
};
CardList.defaultProps = {
	list: []
};

export default CardList;

const ListWrapStyle = styled.div`
	display: flex;
	flex-wrap: wrap;
`;