import React, { Component } from 'react';

import styled from 'styled-components';
import style from 'components/utils';

class TextContent extends Component {
	render () {
		return (
			<TextStyle>
				{this.props.children}
			</TextStyle>
		);
	}
}


export default TextContent;

const TextStyle = styled.p`
	color: ${style.color.bg2};
	text-align:center;
	margin-bottom: 5px;
`;