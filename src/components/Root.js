import React, {Component, Fragment} from 'react';

import App from './App';

class Root extends Component {
	render() {
		return (
			<Fragment >
				<App />
			</Fragment>
		)
	}
}

export default Root