import * as color from './color';
import * as indents from './indents'
export default {
	color,
	indents
};