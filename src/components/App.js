import React, {Component} from 'react';
import styled from 'styled-components';

import AppWrap from 'containers/AppWrap';
import CardListWrap from 'containers/CardListWrap';
import Select from 'containers/Select';

class App extends Component {
	render() {
			return (
			<AppStyle>
				<AppWrap>
					<Select />
					<CardListWrap />
				</AppWrap>
			</AppStyle>
		)
	};
}
export default App;

const AppStyle = styled.div`
	display:flex;
`;

