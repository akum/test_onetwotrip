import {Record, OrderedMap} from 'immutable';
import { createSelector } from "reselect";
import {all, put, call, takeEvery} from 'redux-saga/effects';
//config???
const appName = 'OneTwoTrip';
// end config
const ReducerRecord = Record({
	list: new OrderedMap({}),
	selected: 'all'
});

const FlightRecod = Record({
	arrival   : null,
	carrier   : null,
	departure : null,
	direction : null,
	diffTime  : null,
	id        : null
});

export const moduleName = 'flight';

const path = `${appName}/${moduleName}`;

export const GET_LIST_REQUEST = `${path}/GET_LIST_REQUEST`;
export const GET_LIST_SUCCESS = `${path}/GET_LIST_SUCCESS `;

//Не фильтр а выбор
export const FILTER_SELECT = `${path}/SELECT`;
export const FILTER_SELECT_SUCCESS = `${path}/SELECT_SUCCESS`;

/**
 * REDUCE
 */
export default function flightsReducer(state = new ReducerRecord(), action) {
	const {type, payload} = action;
	switch (type) {
		case GET_LIST_SUCCESS:
			return state.set('list', new OrderedMap(payload).mapEntries( ( [id, val] ) => (
				[id, new FlightRecod(val)]
			)));
		case FILTER_SELECT_SUCCESS:
			return state.set('selected', payload);
		default:
			return state;
	}
};
/**
 * END REDUCE
 */

/**
 * ACTIONS
 */
export function getList() {
	return {
		type    : GET_LIST_REQUEST,
	}
};

export function activeSelect(selected) {
	return {
		type: FILTER_SELECT,
		payload: {selected}
	}
};

/**
 * END ACTIONS
 */

/**
 * SELECTORS
 */
	//utils.js
const diffTime = (d1, d2) =>{
	let time = 60*60;
	if(d2 < d1) [d1, d2] = [d2, d1]
	
	let diff = Math.abs(Math.floor((d2 - d1)/1000));
	let diffDate = {};
	
	// diffDate['day'] = Math.floor(diff/(day*time));
	// diff -= diffDate['day']*(day*time);
	
	diffDate['hours'] = Math.floor( (diff)/(time));
	diff -= diffDate['hours']*(time);
	
	diffDate['min'] = Math.round((diff) / 60);
	return diffDate;
};
const timeFormat = (date) =>{
	const formatOpt = {
		year: '2-digit',
		month: '2-digit',
		day: '2-digit',
		hour: '2-digit',
		minute: '2-digit',
	};
	return date.toLocaleString("ru", formatOpt);
};

export const stateSelector = state => state[moduleName];
export const getListSelector = createSelector(stateSelector, state => state.list);
export const getActiveSelector = createSelector(stateSelector, state => state.selected);

export const filteredList = createSelector(getActiveSelector, getListSelector, (selected, val) => val.map( val => {
	//utils
	const resTime = diffTime(new Date(val.departure), new Date(val.arrival));
	
	let obj = val.toJS();
	obj['departure'] = timeFormat(new Date(val.departure));
	obj['arrival'] =  timeFormat(new Date(val.arrival));
	obj['diffTime'] = `Часов: ${resTime.hours} Минут: ${resTime.min}`;
	return new FlightRecod(obj);
}).filter(value=> selected && selected !== 'all' ? value.carrier === selected: value));

export const getCarierSelector = createSelector(getListSelector, list => list
	.valueSeq()
	.toArray()
	.map((val) => (
		val.carrier
	))
);

export const getCarierUniqSelector = createSelector(getCarierSelector, carrier => carrier
	.filter( (val, idx, arr ) => arr.indexOf(val) === idx)
);
/**
 * END SELECTORS
 */

/**
 * SAGA
 */
export const getListSaga = function * () {
	try {
		const res = yield call(fetch, "data.json");
		const {flights} = JSON.parse(yield call([res, res.text]));
		
		let data = flights.reduce((map, obj) => {
			map[obj.id] = obj;
			return map;
		}, {});
		
		yield put({
			type: GET_LIST_SUCCESS,
			payload: data
		});
	} catch (err) {
		console.log('error', err)
	}
};


export const filterSelectSaga = function * (action) {
	const {selected} = action.payload;
	yield put({
		type: FILTER_SELECT_SUCCESS,
		payload: selected
	});
};

export const saga = function * (){
	yield all([
		takeEvery(GET_LIST_REQUEST, getListSaga),
		takeEvery(FILTER_SELECT, filterSelectSaga),
	])
};

/**
 * END SAGA
 */
